<?php

function friendselectric_wrap_content($text) {
  $text = preg_replace('!<pre>!i', '<div class="pre"><pre>', $text);
  $text = preg_replace('!</pre>!i', '</pre></div>', $text);
  return $text;
}
function friendselectric_wrap_links($link, $n) {
  $classes = array("lw1", "lw2");
  $before = $after = "";
  foreach ($classes as $c) {
    $before .= '<span class="'. $c .'">';
    $after .= '</span>';
  }
  $link = preg_replace('!<a[^>]*>!i', '\0'. $before, $link);
  $link = preg_replace('!</a[^>]*>!i', $after . '\0', $link);
  return $link;
}

function friendselectric_menu_item_link($link) {
  //  $link = l('<span class="lw1">'. check_plain($link['title']) .'</span>', $link['href'], array('html' => TRUE));
    if (empty($link['options'])) {
    $link['options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="lw1">' . check_plain($link['title']) . '</span>';
    $link['options']['html'] = TRUE;
  }

  if (empty($link['type'])) {
    $true = TRUE;
  }

  return l($link['title'], $link['href'], $link['options']);

  return $link;
}


function phptemplate_menu_primary_links($primary_links) {
  foreach ($primary_links as $link) {
    $links[] = l('<span class="lw3">'. check_plain($link['title']) .'</span>', $link['href'], array('html' => TRUE));
  }

  return $links;
}

function friendselectric_comment_thread_collapsed($comment) {
  if ($comment->depth) {
    $output  = '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
    $output .= theme('comment_view', $comment, '', 0);
    $output .= "</div>\n";
  }
  else {
    $output .= theme('comment_view', $comment, '', 0);
  }
  return $output;
}

function friendselectric_comment_thread_expanded($comment) {
  $output = '';
  if ($comment->depth) {
    $output .= '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
  }

  $output .= theme('comment_view', $comment, module_invoke_all('link', 'comment', $comment, 0));

  if ($comment->depth) {
    $output .= "</div>\n";
  }
  return $output;
}


/**
 * Override or insert PHPTemplate variables into the page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("page" in this case.)
 */
function friendselectric_preprocess_page(&$vars, $hook) {
}

/**
 * Return the path to the main nista theme directory.
 */
function path_to_friendselectrictheme() {
  static $theme_path;
  if (!isset($theme_path)) {
    global $theme;
    if ($theme == 'nista') {
      $theme_path = path_to_theme();
    }
    else {
      $theme_path = drupal_get_path('theme', 'friendselectric');
    }
  }
  return $theme_path;
}

